# Team Formation

## _Draft Your Squad_



Implemented using

- laravel 10.10
- php Version 8.3.2

## Features

- This Laravel project allows you to manage your team formation by adding , editing and deleting your squad players.
and if you don't have a squad you can create your own team and enjoy.
- Firebase integration is a key feature, with Cloud Firestore being utilized to synchronize data with the MySQL database.

## Installation

Clone repo the dependencies and devDependencies.

```sh
git clone https://gitlab.com/abdelrahman.amin/team-formation.git
cd team-formation
```

Install composer dependencies Using docker.

```sh
docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v "$(pwd):/var/www/html" \
    -w /var/www/html \
    laravelsail/php82-composer:latest \
    composer install --ignore-platform-reqs
```

Copy the .env.example file to .env

```sh
cp .env.example .env
```

Start the Docker containers

```sh
./vendor/bin/sail up -d
```

Generate an application key

```sh
./vendor/bin/sail artisan key:generate
```

Run the database migrations and seeding

```sh
./vendor/bin/sail artisan migrate --seed
```

## Firebase Setup Instructions

To get started, follow these setup instructions:

1. **Firebase Configuration:**
   - Create a project on [Google Firebase](https://console.firebase.google.com/).
   - Create Firestone storage.
   - Obtain your Firebase project credentials.
   - Include your Firebase app credentials in the `.env` file of your Laravel project.

   ```env
   FIREBASE_CREDENTIALS=path-to-your-api-key-auth-file.json

## APIs

**Display all players and one player by ID**

| Method | Url |
| ------ | ------ |
| GET | /api/players |
| GET | /api/players/{id} |

- **Data Synchronization Approaches:**
  Data synchronization between MySQL and Firestore can be achieved through various approaches. In this project, the chosen method is the Synchronize approach with eloquent observers. Another option is to implement queuing and background processing mechanisms.

  - **Synchronize Approach with Eloquent Observers:**
    This approach involves Laravel Eloquent Observers to automatically synchronize data between the MySQL database and Firestore. Eloquent Observers allow for streamlined handling of model events, ensuring seamless updates to Firestore whenever relevant changes occur in the Laravel application.

  - **Queuing and Background Processing:**
    An alternative method involves using queuing and background processing to handle data synchronization tasks. Queues enable asynchronous processing, allowing you to offload time-consuming operations to background workers, enhancing the overall performance and responsiveness of the application.

Choose the synchronization approach that aligns best with your project's requirements and performance considerations.

