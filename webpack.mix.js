
const mix = require('laravel-mix');

mix
    .setPublicPath('public')
    .styles('resources/css/styles.css', 'public/css/styles.css')
    .version();