<?php

namespace App;

interface Constants
{
    public const TEAMS_COLLECTION = 'teams';
    public const PLAYERS_COLLECTION = 'players';
}
