<?php

namespace App\Services\Firebase\Firestore;

use App\Services\Firebase\Firestore\FirestoreInterface;
use Illuminate\Support\Collection;
use Kreait\Firebase\Contract\Firestore;

class FirestoreService implements FirestoreInterface
{
    public function __construct(
        private readonly Firestore $firestore
    ) {
    }

    public function upsert(string $collection, string $documentId, array $data): array
    {
        return $this->firestore
            ->database()
            ->collection($collection)
            ->document($documentId)
            ->set($data);
    }

    public function delete(string $collection, string $documentId): void
    {
        $this->firestore
            ->database()
            ->collection($collection)
            ->document($documentId)
            ->delete();
    }
}
