<?php

namespace App\Services\Firebase\Firestore;

use Illuminate\Support\Collection;

interface FirestoreInterface
{
    /**
     * Upsert Collection Item
     *
     * @var string $collection
     * @var string $documentId
     * @var array $data
     *
     * @return array
     */
    public function upsert(string $collection, string $documentId, array $data): array;

    /**
     * Delete Existing Collection
     *
     * @var string $collection
     * @var string $documentId
     *
     * @return void
     */
    public function delete(string $collection, string $documentId): void;
}
