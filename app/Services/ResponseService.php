<?php

namespace App\Services;

use Illuminate\Http\JsonResponse;

class ResponseService
{
    public static function successResponse($data = [], $message = 'Success', $statusCode = 200): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'data' => $data
        ], $statusCode);
    }

    public static function errorResponse(string $message = 'Error', int $statusCode = 400): JsonResponse
    {
        return response()->json([
            'message' => $message
        ], $statusCode);
    }
}
