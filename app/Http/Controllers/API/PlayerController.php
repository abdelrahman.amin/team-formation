<?php

namespace App\Http\Controllers\API;

use App\Models\Player;
use Illuminate\Http\JsonResponse;
use App\Services\ResponseService;
use App\Http\Controllers\Controller;

class PlayerController extends Controller
{
    public function __construct(
        private readonly ResponseService $responseService
    ) {
    }

    public function index(): JsonResponse
    {
        $players = Player::paginate(10);

        return $this->responseService->successResponse(data: $players, statusCode: 200);
    }

    public function show($id)
    {
        $player = Player::findOrFail($id);

        return $this->responseService->successResponse(data: $player, statusCode: 200);
    }
}
