<?php

namespace App\Http\Controllers;

use Illuminate\View\View;
use App\Models\Team;

class HomeController extends Controller
{
    public function index(): View
    {
        $teams = Team::all();
        return view('welcome', compact('teams'));
    }
}
