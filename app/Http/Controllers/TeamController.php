<?php

namespace App\Http\Controllers;

use App\Models\Team;
use Illuminate\View\View;
use App\Http\Requests\TeamRequest;
use App\Services\Firebase\Firestore\FirestoreService;


class TeamController extends Controller
{
    public function __construct(
        private readonly Team $team,
    ) {
    }
    public function show(Team $team)
    {
        $team->load('players');
        return view('teams.show', compact('team'));
    }

    public function create(): View
    {
        return view("teams.create");
    }

    public function store(TeamRequest $request): View
    {
        $team = $this->team->create($request->all());
        return view('teams.show', compact('team'));
    }
}
