<?php

namespace App\Http\Controllers;

use App\Http\Requests\PlayerRequest;
use App\Models\Player;
use Illuminate\Http\Request;
use App\Models\Team;
use Illuminate\View\View;

class PlayerController extends Controller
{
    public function __construct(
        private readonly Player $player,
        private readonly Team $team,
    ) {
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Request $request)
    {
        $request->validate([
            'team_id' => 'required',
        ]);

        $team = $this->team->findOrFail($request->team_id);

        return view('players.create', compact('team'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PlayerRequest $request)
    {

        $player = $this->player->create($request->all());
        $team = $this->team->findOrFail($request->team_id);
        $team->players()->save($player);

        return redirect()->route('teams.show', ['team' => $team]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Player $player): View
    {
        return view('players.edit', compact('player'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PlayerRequest $request, Player $player)
    {
        $player->update($request->all());

        return redirect()->route('teams.show', ['team' => $player->team_id]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Player $player)
    {
        $player->delete();

        return redirect()->route('teams.show', ['team' => $player->team_id]);
    }
}
