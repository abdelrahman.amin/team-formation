<?php

namespace App\Observers;

use App\Models\Player;
use App\Services\Firebase\Firestore\FirestoreService;
use App\Constants;

class PlayerObserver
{
    public function __construct(
        private readonly FirestoreService $firestore
    ) {
    }

    /**
     * Handle the Player "created" event.
     */
    public function created(Player $player): void
    {
        $this->firestore->upsert(collection: Constants::PLAYERS_COLLECTION, documentId: $player->id, data: $player->toArray());
    }

    /**
     * Handle the Player "updated" event.
     */
    public function updated(Player $player): void
    {
        $this->firestore->upsert(collection: Constants::PLAYERS_COLLECTION, documentId: $player->id, data: $player->toArray());
    }

    /**
     * Handle the Player "deleted" event.
     */
    public function deleted(Player $player): void
    {
        $this->firestore->delete(collection: Constants::PLAYERS_COLLECTION, documentId: $player->id);
    }
}
