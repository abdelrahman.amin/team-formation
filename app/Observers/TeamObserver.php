<?php

namespace App\Observers;

use App\Constants;
use App\Models\Team;
use App\Services\Firebase\Firestore\FirestoreService;

class TeamObserver
{
    public function __construct(
        private readonly FirestoreService $firestore
    ) {
    }

    /**
     * Handle the Team "created" event.
     */
    public function created(Team $team): void
    {
        $this->firestore->upsert(collection: Constants::TEAMS_COLLECTION, documentId: $team->id, data: $team->toArray());
    }
}
