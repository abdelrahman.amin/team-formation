<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Draft Your Squad</title>
    @vite(['resources/css/styles.css', 'resources/js/app.js'])

</head>

<body>
    <div class="container">
        <header>
            <h1>Draft Your Squad</h1>
            <div class="info">
                <span>O. Fanz 4-3-3</span>
                <span>100M</span>
            </div>
        </header>
        <main>

            <div class="deadline">
                <p>Game Week 1 Deadline 11 Aug 2023 at 20:30</p>
            </div>
            @if (isset($team))
                <div>
                    <div>
                        <h2>Your Team:{{ $team->name }}</h2>
                    </div>
                </div>
                <div class="squad">

                    @foreach ($team->players as $player)
                        <div class="position">
                            <p>{{ $player->position }}</p>
                            <p>{{ $player->name }}</p>

                            <div class="player-actions">
                                <a href="{{ route('players.edit', ['player' => $player->id]) }}"><button class="main-button" type="submit">Edit</button></a>
                                <form method="post" action="{{ route('players.destroy', ['player' => $player->id]) }}">
                                    @csrf
                                    @method('DELETE')
                                    <button class="main-button" type="submit">Delete</button>
                                </form>
                            </div>
                        </div>
                    @endforeach
                    @for ($i = 1; $i <= 11 - count($team->players); $i++)
                        <div class="position placeholder">
                            <p>+ ADD</p>
                            <a href="{{ route('players.create', ['team_id' => $team->id, 'position' => 'FWD']) }}">
                                <button>+ ADD FWD</button>
                            </a>
                        </div>
                    @endfor
                </div>

        </main>
        <footer>
            <span>fanz</span>
        </footer>
    @else
        <p>No team data available.</p>
        <a href="{{ route('teams.create') }}"><button class="main-button">Create Your Team</button></a>
        @endif
    </div>
</body>

</html>
