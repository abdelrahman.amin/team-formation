<!-- resources/views/players/edit.blade.php -->

<!DOCTYPE html>
<html lang="en">

<head>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            background-color: #220d3a;
        }

        form {
            background-color: #bfb7eb;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            box-sizing: border-box;
        }

        button {
            background-color: #451e71;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button:hover {
            background-color: #973eff;
        }
    </style>
</head>

<body>
    <form method="post" action="{{ route('players.update', ['player' => $player->id]) }}">
        @csrf
        @method('PUT')


        <input type="hidden" name="team_id" value="{{ $player->team_id }}">
        <label for="name">Player Name:</label>
        <input type="text" id="name" name="name" value="{{ old('number', $player->name) }}" required>

        <label for="age">Player Age:</label>
        <input type="text" id="age" name="age" value="{{ old('number', $player->age) }}" required>

        <label for="age">Player Position:</label>
        <input type="text" id="position" name="position" value="{{ old('number', $player->position) }}" required>

        <label for="age">Player Number:</label>
        <input type="text" id="number" name="number" value="{{ old('number', $player->number) }}" required>

        <button type="submit">Update Player</button>

    </form>
</body>

</html>
