<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Create a New Player</title>
    <style>
        body {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
            margin: 0;
            background-color: #220d3a;
        }

        form {
            background-color: #bfb7eb;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
            text-align: center;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            box-sizing: border-box;
        }

        button {
            background-color: #451e71;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        button:hover {
            background-color: #973eff;
        }
    </style>
</head>

<body>

    <form method="post" action="{{ route('players.store') }}">
        @csrf
        <input type="hidden" name="team_id" value="{{ $team->id }}">
        <label for="name">Player Name:</label>
        <input type="text" id="name" name="name" placeholder="enter player name" required>
        <label for="age">Player Age:</label>
        <input type="text" id="age" name="age" placeholder="enter player age" required>
        <label for="age">Player Position:</label>
        <input type="text" id="position" name="position" placeholder="enter player position" required>
        <label for="age">Player Number:</label>
        <input type="text" id="number" name="number" placeholder="enter player number" required>
        <button type="submit">Create Player</button>
    </form>
</body>

</html>
