<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>FANZ</title>

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.bunny.net">
    <link href="https://fonts.bunny.net/css?family=figtree:400,600&display=swap" rel="stylesheet" />

    <!-- Styles -->
    <style>
        body {
            margin: 0;
            padding: 0;
            height: 100vh;
            display: flex;
            justify-content: center;
            align-items: center;
            background-color: #220d3a;

        }

        .container {
            text-align: center;
        }

        .main-button {
            font-size: 20px;
            padding: 15px 30px;
            border: none;
            border-radius: 5px;
            background-color: #451e71;
            color: #fff;
            cursor: pointer;
        }

        .main-button:hover {
            background-color: #973eff;
        }

        h1 {
            color: #ffb700;
        }

        td,
        tr {
            color: #fff;
        }
    </style>
</head>

<body class="antialiased">
    <div class="container">
        <h1>Welcome</h1>
        <a href="{{ route('teams.create') }}"><button class="main-button">Create Your Team</button></a>

        <h1>All Teams:</h1>

        <table border="1">
            <thead>
                <tr>
                    <th>Team Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($teams as $team)
                    <tr>
                        <td>{{ $team->name }}</td>
                        <td>
                            <a href="{{ route('teams.show', ['team' => $team->id]) }}">View Team</a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</body>

</html>
